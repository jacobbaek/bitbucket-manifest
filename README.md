# bitbucket manifests

# bitbucket envirnonment variables
This asssumed the pv already existed.
## steps to install bitbucket server on the kubernetes
1. kubectl create -f bitbucket-pvc.yaml (if have the pv manifest, then should make first)
2. kubectl create -f bitbucket-deployment.yaml
3. kubectl create -f bitbucket-svc.yaml
## references
 * https://hub.docker.com/r/atlassian/bitbucket-server/

# postgresql manifests
This manifest should use the configmap.
There is secret manifest. but it is not tested by me.
## steps to install postgresql on the kubernetes
1. kubectl create -f postgres-pvc.yaml (if have the pv manifest, then should make first)
2. kubectl create -f postgres-configmap.yaml
3. kubectl create -f postgres-deployment.yaml
4. kubectl create -f postgres-svc.yaml
## references
 * https://severalnines.com/database-blog/using-kubernetes-deploy-postgresql

# nginx manifests
This manifest will be used reverse proxy for bitbucket

1. kubectl create -f nginx-rproxy-configmap.yaml
2. kubectl create -f nginx-rproxy-deployment.yaml
3. kubectl create -f nginx-rproxy-svc.yaml
